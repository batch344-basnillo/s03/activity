import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int num = 0;
        int answer = 1;
        int counter = 1;
        boolean isInputError = false;

        System.out.println("Input an integer whose factorial will be computed:");
        try{
            num = in.nextInt();
        } catch (Exception e){
            isInputError = true;
            System.out.println("Invalid Input");
            e.printStackTrace();
        } finally {
            if ( num == 0 ){
                if( isInputError == false ) {
                    System.out.println("The factorial of 0 is 1.");
                }
            } else if ( num < 0 ){
                System.out.println("The factorial of negative numbers are not defined here.");
            } else {
                //While Loop
                while ( counter <= num ){
                    answer *= counter;
                    counter++;
                }
                System.out.println("The factorial of " + num + " using a While loop is " + answer);

                //For Loop
                for(counter = 1, answer = 1; counter <= num; counter++){
                    answer *= counter;
                }
                System.out.println("The factorial of " + num + " using a For loop is " + answer);
            }
        }
    }
}